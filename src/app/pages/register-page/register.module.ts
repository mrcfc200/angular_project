import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterContainerComponent } from './components/register-container/register-container.component';
import { RegisterContentComponent } from './components/register-container/register-content/register-content.component';
import { RegisterFooterComponent } from './components/register-container/register-footer/register-footer.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RegisterContainerComponent,
    RegisterContentComponent,
    RegisterFooterComponent,
  ],
  imports: [CommonModule, RegisterRoutingModule, ReactiveFormsModule],
})
export class RegisterModule {}
