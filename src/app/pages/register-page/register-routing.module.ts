import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterContainerComponent } from './components/register-container/register-container.component';
import { RegisterModule } from './register.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RegisterContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterRoutingModule {}
