import { FormGroup } from '@angular/forms';

export function passwordMatch(password: string, passwordRepeated: string) {
  return (formGroup: FormGroup) => {
    const passwordControl = formGroup.controls[password];
    const passwordRepeatedControl = formGroup.controls[passwordRepeated];

    if (
      passwordRepeatedControl.errors &&
      !passwordRepeatedControl.errors['mustMatch']
    ) {
      return;
    }

    if (passwordControl.value !== passwordRepeatedControl.value) {
      console.log(passwordControl.value, passwordRepeatedControl.value);

      passwordRepeatedControl.setErrors({ mustMatch: true });
    } else {
      passwordRepeatedControl.setErrors(null);
    }
  };
}
