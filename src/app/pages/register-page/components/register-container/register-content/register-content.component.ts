import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { passwordMatch } from '../../../models/validator';


@Component({
  selector: 'app-register-content',
  templateUrl: './register-content.component.html',
  styleUrls: ['./register-content.component.scss'],
})
export class RegisterContentComponent implements OnInit {
  public registerForm!: FormGroup;
  public submitted: boolean = false;
  constructor(private formBuilder: FormBuilder) {
    this.registerForm = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.maxLength(15)]],
        passwordRepeated: ['', [Validators.required, Validators.maxLength(15)]],
      },
      {
        validator: passwordMatch('password', 'passwordRepeated'),
      }
    );
  }

  ngOnInit(): void {}

  public onSubmit(): void {
    this.submitted = true;
  }

  
}
