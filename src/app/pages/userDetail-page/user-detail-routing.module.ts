import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDetailContainerComponent } from './components/userDetail-container/user-detail-container.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: UserDetailContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserDetailRoutingModule {}
