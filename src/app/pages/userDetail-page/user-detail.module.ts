import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserDetailRoutingModule } from './user-detail-routing.module';
import { UserDetailContainerComponent } from './components/userDetail-container/user-detail-container.component';
import { UserDetailContentComponent } from './components/userDetail-container/userDetail-content/user-detail-content.component';
import { UserDetailPhotosComponent } from './components/userDetail-container/userDetail-content/userDetail-photos/user-detail-photos.component';

@NgModule({
  declarations: [UserDetailContainerComponent, UserDetailContentComponent, UserDetailPhotosComponent],
  imports: [CommonModule, UserDetailRoutingModule],
})
export class UserDetailModule {}
