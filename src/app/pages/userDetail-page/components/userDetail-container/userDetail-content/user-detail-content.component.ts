import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { userDetailModel } from 'src/app/shared/models/userDetailModel';
import { DataApiService } from 'src/app/shared/services/data-api/data-api.service';

@Component({
  selector: 'app-user-detail-content',
  templateUrl: './user-detail-content.component.html',
  styleUrls: ['./user-detail-content.component.scss'],
})
export class UserDetailContentComponent implements OnInit {
  userParam: string | null = '';
  userList: userDetailModel[] = [];
  userGallery: Array<string> = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private dataApi: DataApiService
  ) {}

  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.userParam = this.activatedRoute.snapshot.queryParamMap.get('user');
    this.dataApi.getUserDetails(this.userParam).subscribe((userData: any) => {
      const results: userDetailModel[] = [];
      console.log(userData);

      results.push(userData);

      const formattedResults = results.map(
        ({
          username,
          bio,
          profile_image: { large: profileImage },
          social: {
            instagram_username: instagramLink,
            twitter_username: twitterLink,
          },
          photos: gallery,
        }: any) => ({
          username,
          bio,
          profileImage,
          instagramLink,
          twitterLink,
          gallery,
        })
      );
      this.userList = formattedResults;

      formattedResults[0]['gallery'].forEach((item: any) => {
        this.userGallery.push(item['urls']['regular']);
      });
    });
  }
}
