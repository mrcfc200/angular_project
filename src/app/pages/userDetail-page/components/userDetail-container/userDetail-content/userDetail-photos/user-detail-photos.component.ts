import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-detail-photos',
  templateUrl: './user-detail-photos.component.html',
  styleUrls: ['./user-detail-photos.component.scss'],
})
export class UserDetailPhotosComponent implements OnInit {
  @Input() userGallery: Array<string> = [];
  displayPhotos: boolean = false;
  constructor() {}

  ngOnInit() {}

  display() {
    this.displayPhotos = !this.displayPhotos;
  }
}
