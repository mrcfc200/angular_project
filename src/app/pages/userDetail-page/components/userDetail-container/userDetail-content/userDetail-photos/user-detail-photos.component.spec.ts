import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailPhotosComponent } from './user-detail-photos.component';

describe('UserDetailPhotosComponent', () => {
  let component: UserDetailPhotosComponent;
  let fixture: ComponentFixture<UserDetailPhotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDetailPhotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
