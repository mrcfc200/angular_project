import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  public searchDataInput: string = '';
  public filterPhoto: string = '';
  constructor() {}

  ngOnInit(): void {}

  sendSearchInput(event: any) {
    this.searchDataInput = event.target.value;
  }
}
