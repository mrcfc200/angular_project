import { Component, Input, OnInit, Output } from '@angular/core';
import { photoModel } from 'src/app/shared/models/photoModel';
import { DataApiService } from 'src/app/shared/services/data-api/data-api.service';
import { Router } from '@angular/router';
import { AddFavoritesService } from 'src/app/shared/services/add-favorites/add-favorites.service';

@Component({
  selector: 'app-search-content',
  templateUrl: './search-content.component.html',
  styleUrls: ['./search-content.component.scss'],
})
export class SearchContentComponent implements OnInit {
  @Input() searchDataInput: string = '';
  @Input() filterPhoto: string = '';
  foundPhoto: boolean = true;
  photoList: photoModel[] = [];
  dataRaw: photoModel[] = [];
  favoritePhotos: Array<photoModel> = [];

  constructor(
    private dataApi: DataApiService,
    private router: Router,
    private handdleFavorites: AddFavoritesService
  ) {}

  ngOnInit() {
    this.getAllData();
    console.log(this.searchDataInput);
  }

  getAllData() {
    for (let i = 1; i < 3; i++) {
      this.dataApi.getDefaultContent(i).subscribe((data: any) => {
        const results: photoModel[] = data;
        const formattedResults = results.map(
          ({
            user: { username, profile_image: imgProfile },
            urls: { regular: imgPhoto },
            description,
          }: any) => ({
            username,
            imgPhoto,
            imgProfile,
            description,
          })
        );

        formattedResults.forEach((photo) => {
          this.photoList.push(photo);
        });
      });
    }
  }

  goUserDetail(username: string) {
    this.router.navigate(['/profile', this.favoritePhotos], {
      queryParams: { user: username },
    });
  }

  addToFavorites(item: photoModel) {
    this.handdleFavorites.handdleFavorites(item);
  }
}
