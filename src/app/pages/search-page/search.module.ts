import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchContainerComponent } from './components/search-container/search-container.component';
import { SearchBarComponent } from './components/search-container/search-bar/search-bar.component';
import { SearchContentComponent } from './components/search-container/search-bar/search-content/search-content.component';
import { FormsModule } from '@angular/forms';
import { SearchPhotoPipe } from './pipes/search-photo.pipe';

@NgModule({
  declarations: [
    SearchContainerComponent,
    SearchBarComponent,
    SearchContentComponent,
    SearchPhotoPipe,
  ],
  imports: [CommonModule, SearchRoutingModule, FormsModule],
})
export class SearchModule {}
