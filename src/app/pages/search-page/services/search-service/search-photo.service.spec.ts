import { TestBed } from '@angular/core/testing';

import { SearchPhotoService } from './search-photo.service';

describe('SearchPhotoService', () => {
  let service: SearchPhotoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchPhotoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
