import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPhoto',
})
export class SearchPhotoPipe implements PipeTransform {
  transform(value: any, arg: any): any {
    const resultPhoto = [];
    if (arg) {
      for (let photo of value) {
        if (photo.description) {
          if (photo.description.toLowerCase().includes(arg.toLowerCase())) {
            resultPhoto.push(photo);
          }
        } else if (photo.username) {
          if (photo.username.toLowerCase().includes(arg.toLowerCase())) {
            resultPhoto.push(photo);
          }
        }
      }

      return resultPhoto;
    } else {
      return value;
    }
  }
}
