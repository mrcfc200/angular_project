import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorites-container',
  templateUrl: './favorites-container.component.html',
  styleUrls: ['./favorites-container.component.scss']
})
export class FavoritesContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
