import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritesContentComponent } from './favorites-content.component';

describe('FavoritesContentComponent', () => {
  let component: FavoritesContentComponent;
  let fixture: ComponentFixture<FavoritesContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FavoritesContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
