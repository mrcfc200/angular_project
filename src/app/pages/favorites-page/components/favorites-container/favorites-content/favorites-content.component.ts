import { Component, OnInit } from '@angular/core';
import { photoModel } from 'src/app/shared/models/photoModel';

import { AddFavoritesService } from 'src/app/shared/services/add-favorites/add-favorites.service';

@Component({
  selector: 'app-favorites-content',
  templateUrl: './favorites-content.component.html',
  styleUrls: ['./favorites-content.component.scss'],
})
export class FavoritesContentComponent implements OnInit {
  favoritePhotos: Array<photoModel> = [];
  constructor(private handdleFavorites: AddFavoritesService) {
    this.favoritePhotos = this.handdleFavorites.getFavorites();
    console.log(this.favoritePhotos);
  }

  ngOnInit() {}
}
