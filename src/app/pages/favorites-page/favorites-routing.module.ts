import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoritesContainerComponent } from './components/favorites-container/favorites-container.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FavoritesContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoritesRoutingModule {}
