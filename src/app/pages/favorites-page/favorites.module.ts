import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FavoritesRoutingModule } from './favorites-routing.module';
import { FavoritesContainerComponent } from './components/favorites-container/favorites-container.component';
import { FavoritesContentComponent } from './components/favorites-container/favorites-content/favorites-content.component';

@NgModule({
  declarations: [
    FavoritesContainerComponent,
    FavoritesContentComponent
  ],
  imports: [CommonModule, FavoritesRoutingModule],
})
export class FavoritesModule {}
