import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeContainerComponent } from './components/home-container/home-container.component';
import { HomeContentComponent } from './components/home-container/home-content/home-content.component';
import { HomeHeaderComponent } from './components/home-container/home-header/home-header.component';

@NgModule({
  declarations: [
    HomeContainerComponent,
    HomeContentComponent,
    HomeHeaderComponent
  ],
  imports: [CommonModule, HomeRoutingModule],
})
export class HomeModule {}
