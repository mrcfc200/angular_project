import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/shared/services/data-api/data-api.service';

@Component({
  selector: 'app-home-content',
  templateUrl: './home-content.component.html',
  styleUrls: ['./home-content.component.scss'],
})
export class HomeContentComponent implements OnInit {
  randomPhoto: string = '';

  constructor(private dataApi: DataApiService) {}

  ngOnInit(): void {}

  getRandomPhoto() {
    this.dataApi.getRandomContent().subscribe((data: any) => {
      const {
        urls: { regular: url },
      } = data;
      this.randomPhoto = url;
    });
  }
}
