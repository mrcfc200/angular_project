import { Component, OnInit } from '@angular/core';
import { photoModel } from 'src/app/shared/models/photoModel';
import { DataApiService } from 'src/app/shared/services/data-api/data-api.service';

@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
export class ExploreContainerComponent implements OnInit {
  photoList: photoModel[] = [];
  activePreviousButton: boolean = false;
  paginatorCounter: number = 1;
  dataRaw: Array<string> = [];
  constructor(private dataApi: DataApiService) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.dataApi
      .getDefaultContent(this.paginatorCounter)
      .subscribe((data: any) => {
        const results: photoModel[] = data;

        const formattedResults = results.map(
          ({
            user: { username, profile_image: imgProfile },
            urls: { regular: imgPhoto },
          }: any) => ({
            username,
            imgPhoto,
            imgProfile,
          })
        );
        this.photoList = formattedResults;
      });
  }

  clickPaginatorNext() {
    this.activePreviousButton = true;
    this.paginatorCounter += 1;
    console.log(this.paginatorCounter);
    this.getData();
  }

  clickPaginatorPrevious() {
    this.paginatorCounter -= 1;
    if (this.paginatorCounter <= 1) {
      this.activePreviousButton = false;
    }
    this.getData();
  }
}
