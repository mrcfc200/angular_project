import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExploreContainerComponent } from './components/explore-container/explore-container.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ExploreContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExploreRoutingModule {}
