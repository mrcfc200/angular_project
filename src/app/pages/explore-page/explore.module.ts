import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExploreRoutingModule } from './explore-routing.module';
import { ExploreContainerComponent } from './components/explore-container/explore-container.component';

@NgModule({
  declarations: [ExploreContainerComponent],
  imports: [CommonModule, ExploreRoutingModule],
})
export class ExploreModule {}
