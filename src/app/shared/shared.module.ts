import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataApiService } from './services/data-api/data-api.service';
import { AddFavoritesService } from './services/add-favorites/add-favorites.service';

@NgModule({
  declarations: [],
  providers: [DataApiService, AddFavoritesService],
  imports: [CommonModule],
  exports: [],
})
export class SharedModule {}
