export interface photoModel {
  username: string;
  imgPhoto: string;
  imgProfile: string;
  description?: string;
  //urlProfile: string;
}
