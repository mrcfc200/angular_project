export interface userDetailModel {
  username: string;
  bio: string;
  profileImage: string;
  instagramLink: string;
  twitterLink: string;
  gallery: Array<object>;
}
