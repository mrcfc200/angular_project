import { ReturnStatement } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { photoModel } from '../../models/photoModel';

@Injectable({
  providedIn: 'root',
})
export class AddFavoritesService {
  favoritesArray: any = [];
  constructor() {}

  handdleFavorites(favorite: photoModel) {
    this.favoritesArray.push(favorite);
  }

  getFavorites() {
    return this.favoritesArray;
  }
}
