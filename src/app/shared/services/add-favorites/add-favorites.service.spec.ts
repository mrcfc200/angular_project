import { TestBed } from '@angular/core/testing';

import { AddFavoritesService } from './add-favorites.service';

describe('AddFavoritesService', () => {
  let service: AddFavoritesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddFavoritesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
