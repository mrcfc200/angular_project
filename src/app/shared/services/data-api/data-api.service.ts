import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataApiService {
  constructor(private http: HttpClient) {}
  getDefaultContent(page: number) {
    return this.http.get(
      `https://api.unsplash.com/photos?page=${page}&client_id=QxZTbjabxBItqIBQTasigLkp5vqkeoKGU2wLDLRXGVM#`
    );
  }

  getUserDetails(user: string | null) {
    return this.http.get(
      `https://api.unsplash.com/users/${user}/?client_id=QxZTbjabxBItqIBQTasigLkp5vqkeoKGU2wLDLRXGVM#`
    );
  }

  getRandomContent() {
    return this.http.get(
      'https://api.unsplash.com/photos/random/?client_id=QxZTbjabxBItqIBQTasigLkp5vqkeoKGU2wLDLRXGVM#'
    );
  }
}

//https://api.unsplash.com/photos?page=1&client_id=QxZTbjabxBItqIBQTasigLkp5vqkeoKGU2wLDLRXGVM#
