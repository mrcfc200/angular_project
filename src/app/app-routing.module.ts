import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  },
  {
    path: 'home',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/home-page/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'explore',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/explore-page/explore.module').then(
        (m) => m.ExploreModule
      ),
  },
  {
    path: 'search',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/search-page/search.module').then((m) => m.SearchModule),
  },
  {
    path: 'register',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/register-page/register.module').then(
        (m) => m.RegisterModule
      ),
  },
  {
    path: 'profile',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/userDetail-page/user-detail.module').then(
        (m) => m.UserDetailModule
      ),
  },
  {
    path: 'favorites',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/favorites-page/favorites.module').then(
        (m) => m.FavoritesModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
